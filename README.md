# Monorepo DAG Sample

A minimal example of a monorepo setup with multiple pipelines using a directed acyclic graph. Contributed by [Aaron Gorka](https://gitlab.com/aarongorka)

Related docs:
* GitLab DAG docs https://docs.gitlab.com/ee/ci/directed_acyclic_graph/
* Monorepo package mgt workflows https://docs.gitlab.com/ee/user/packages/workflows/working_with_monorepos.html
* Monorepo GitLab direction https://about.gitlab.com/direction/monorepos/
